import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddressPageComponent } from './address-page/address-page.component';

const routes: Routes = [
  { path: 'address', component: AddressPageComponent },
  { path: '', redirectTo: '/address', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
