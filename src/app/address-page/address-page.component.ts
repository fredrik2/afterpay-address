import { Component, OnInit } from '@angular/core';
import { Address } from '../components/select-address/address.model';

@Component({
  selector: 'app-address-page',
  templateUrl: './address-page.component.html',
  styleUrls: ['./address-page.component.scss']
})
export class AddressPageComponent implements OnInit {

  country: string;

  constructor() { }

  ngOnInit(): void {
  }

  takeAddress(address: Address) {
    this.country = address.country;
  }

}
