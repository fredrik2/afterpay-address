export class Address {
    streetNr: string;
    street: string;
    town: string;
    zip: string;
    country: string;
}