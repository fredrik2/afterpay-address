import { Component, ElementRef, OnInit, AfterViewInit, ViewChild, NgZone, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import { Address } from './address.model';


@Component({
  selector: 'component-select-address',
  templateUrl: './select-address.component.html',
  styleUrls: ['./select-address.component.scss']
})
export class SelectAddressComponent implements OnInit {

  addressForm: FormGroup;
  addressStr: string;
  addressData = new Address();
  manualAddress = false;

  @ViewChild('search') searchElement: ElementRef;
  private autocomplete: any;

  @Output() address = new EventEmitter();

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) { }

  ngOnInit(): void {
    this.addressForm = new FormGroup({
      'search': new FormControl(''),
      'manuallAddress': new FormGroup({
        'streetNr': new FormControl(''),
        'street': new FormControl(''),
        'zip': new FormControl(''),
        'town': new FormControl(''),
        'country': new FormControl(''),
      })
    });
  }

  ngAfterViewInit() {
    this.mapsAPILoader.load().then(() => {
      this.autocomplete = new google.maps.places.Autocomplete(this.searchElement.nativeElement, {
        types: ["geocode"],
        componentRestrictions: {country: 'se'}
      });

      this.autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = this.autocomplete.getPlace();
          console.log(place);

          if (place.geometry === undefined || place.geometry === null) {
            console.warn('Place changed but no place!');
            this.clearAddress();
            return;
          }

          const components = place.address_components;
          this.addressStr = place.formatted_address || '';
          this.addressData.streetNr = this.getAddresField(components!, 'street_number');
          this.addressData.street = this.getAddresField(components!, 'route');
          this.addressData.town = this.getAddresField(components!, 'postal_town');
          this.addressData.zip = this.getAddresField(components!, 'postal_code');
          this.addressData.country = this.getAddresField(components!, 'country')

          this.address.emit(this.addressData);
        })
      })
    })
  }

  swapManual() {
    this.manualAddress = !this.manualAddress;
  }

  saveAddress() {
    const form = this.addressForm.get('manuallAddress') as FormGroup;
    this.addressData.streetNr = form.controls.streetNr.value;
    this.addressData.street = form.controls.street.value;
    this.addressData.zip = form.controls.zip.value;
    this.addressData.town = form.controls.town.value;
    this.addressData.country = form.controls.country.value;

    this.addressStr = `${this.addressData.streetNr} ${this.addressData.street} ${this.addressData.zip} ${this.addressData.town} ${this.addressData.country}`;
    this.address.emit(this.addressData);  
  }

  clearAddress() {
    this.addressStr = '';
    this.addressData = new Address;
    this.address.emit(this.addressData);
  }

  private getAddresField(components: google.maps.GeocoderAddressComponent[], field: string): string {
    const str = components.find(c => c.types.indexOf(field) !== -1)?.long_name || '';
    console.log(str);
    return str;
  }

}
